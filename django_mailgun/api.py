import urlparse
import urllib

import slumber
from slumber import url_join
from slumber.serialize import BaseSerializer
from django.conf import settings

from django_mailgun import MailgunAPIError


class MailgunAPI(slumber.API):
    def __init__(self, **kwargs):
        access_key = getattr(settings, 'MAILGUN_ACCESS_KEY', '')
        server_name = getattr(settings, 'MAILGUN_SERVER_NAME', '')
        api_url = getattr(settings, 'MAILGUN_API_URL',
                'https://api.mailgun.net/v2')
        slumber_base_url = '%s/%s' % (api_url, server_name)

        kwargs.setdefault('auth', ('api', access_key))
        kwargs.setdefault('base_url', slumber_base_url)
        kwargs.setdefault('append_slash', False)
        kwargs.setdefault('format', 'json')
        kwargs.setdefault('input_format', 'formdata')

        return super(MailgunAPI, self).__init__(**kwargs)
